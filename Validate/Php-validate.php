<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/all.min.css">
    <link rel="stylesheet" href="../css/validate.css">
    <title>PHP Validate</title>
</head>
<body>
    <?php
        require_once "function.php";
        $error = array();
        $error['email'] = '';
        $error['password'] = '';
        if(!empty($_POST)){
            $email = $_POST['email'];
            $password = $_POST['password'];
            if(trim($email,' ') == ''){
                $error['email'] = '<div class="warning">Email không được để trống</div>';
            }else{
                if(checkInput($email, 'email') == false){
                    $error['email'] = '<div class="warning">Email 8-32 ký tự có dạng abc@.gmail.com</div>';
                    $email = '';
                }
            };
            if(trim($password,' ') == ''){
                $error['password'] = '<div class="warning">Password không được để trống</div>';
            }else{
                if(checkInput($password, 'password') == false){
                    $error['password'] = '<div class="warning">Password có 8 ký tự và ít nhất 1 số, 1 chữ hoa, 1 ký tự đặc biệt</div>';
                    $password = '';
                }
            }
        }else{
            $email = '';
            $password = '';
        }
    ?>
	<div class="container h-100">
		<div class="d-flex justify-content-center h-100">
			<div class="user_card">
				<div class="d-flex justify-content-center">
					<div class="brand_logo_container">
						<img src="https://cdn.freebiesupply.com/logos/large/2x/pinterest-circle-logo-png-transparent.png" class="brand_logo" alt="Logo">
					</div>
				</div>
				<div class="d-flex justify-content-center form_container">
					<form action="https://www.google.com/" method="post">
						<div class="input-group flex-column mb-2">
                            <div class="d-flex">
                                <div class="input-group-append">
								    <span class="input-group-text"><i class="fas fa-user"></i></span>
							    </div>
                                <input type="text" name="email" class="form-control input_user" value="<?php echo $email ?>" placeholder="email">
                            </div>
                            <?php echo $error['email']; ?>
						</div>
						<div class="input-group flex-column mb-2">
                            <div class="d-flex">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" name="password" class="form-control input_pass" value="<?php echo $password ?>" placeholder="password">
                            </div>
                            <?php echo $error['password']; ?>
						</div>
						<div class="form-group">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="customControlInline">
								<label class="custom-control-label" for="customControlInline">Remember me</label>
							</div>
						</div>
						<div class="d-flex justify-content-center mt-3 login_container">
				 	        <input type="submit" name="submit" class="btn login_btn" value="Login">
				        </div>
					</form>
				</div>
		
				<div class="mt-4">
					<div class="d-flex justify-content-center links">
						Don't have an account? <a href="" class="ml-2">Sign Up</a>
					</div>
					<div class="d-flex justify-content-center links">
						<a href="">Forgot your password?</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>