<?php
    function checkInput($value, $type = 'email'){
        switch ($type) {
            case 'email':
                $pattern = '#^[a-z][a-z0-9_\.]{4,31}@[[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$#';
                break;
            case 'password':
                $pattern = '#^(?=.*\d)(?=.*[A-Z])(?=.*\W).{8,8}$#';
                break;
            
            default:
                # code...
                break;
        }
        $flag = preg_match($pattern,$value);
        return $flag;
    } 
?>