let email = document.getElementById('email');
let password = document.getElementById('password');

function submitForm() {
	let isValid = true;

	if(email.value !== ''){
		if (email.value.match(/^[a-z][a-z0-9_\.]{4,31}@[[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/) == null) {
			email.parentElement.nextElementSibling.innerHTML = 'Email 8-32 ký tự có dạng abc@.gmail.com';
			email.value = '';
			isValid = false;
		}else{
			email.parentElement.nextElementSibling.innerHTML = '';
		}
	} else {
		email.parentElement.nextElementSibling.innerHTML = 'Email không được để trống';
		isValid = false;
	}
	

	if(password.value !== ''){
		if (password.value.match(/^(?=.*\d)(?=.*[A-Z])(?=.*\W).{8,8}$$/) == null && password.value !== '') {
			password.parentElement.nextElementSibling.innerHTML = 'Password có 8 ký tự và ít nhất 1 số, 1 chữ hoa, 1 ký tự đặc biệt';
			password.value = '';
			isValid = false;
		}else{
			password.parentElement.nextElementSibling.innerHTML = '';
		}
	}else {
		password.parentElement.nextElementSibling.innerHTML = 'Password không được để trống';
		isValid = false;
	}
	return isValid;
}

