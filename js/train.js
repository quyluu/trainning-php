let widthScreen = screen.width;
let heightScreen = screen.height;
let train = document.getElementById("train");
let widthTrain = train.width;
let heighTrain = train.height;
let offsetLeft = train.offsetLeft;
let offsetTop = train.offsetTop;
let goTrain = false;
let btnGo = document.getElementById("go");
let btnPause = document.getElementById("pause");

function play() {
	if(goTrain == true){
		if (offsetLeft < widthScreen - widthTrain && offsetTop == 0 && goTrain == true) {
			setTimeout(function () {
				train.style.transform = "rotate(0deg)";
				offsetLeft += 1;
				train.style.left = offsetLeft + 'px';
				go();
			}, 7)
		} else
			if (offsetLeft == widthScreen - widthTrain && offsetTop <= heightScreen - heighTrain * 2 && goTrain == true) {
				setTimeout(function () {
					train.style.transform = "rotate(90deg)";
					offsetTop += 1;
					train.style.top = offsetTop + 'px';
					go();
				}, 7);
			} else
				if (offsetLeft >= 0 && goTrain == true) {
					setTimeout(function () {
						train.style.transform = "rotateY(180deg)";
						offsetLeft -= 1;
						train.style.left = offsetLeft + 'px';
						go();
					}, 7);
				} else
					if (offsetTop >= 0 && goTrain == true) {
						setTimeout(function () {
							train.style.transform = "rotate(270deg)";
							offsetTop -= 1;
							train.style.top = offsetTop + 'px';
							go();
						}, 7);
					}
	}
}


function pause() {
	btnPause.classList.add("disable");
	btnGo.classList.remove("disable");
	goTrain = false;
	document.getElementById('bg-music').pause();
}
function start(){
	btnGo.classList.add("disable");
	btnPause.classList.remove("disable");
	goTrain = true;
	go()
	document.getElementById('bg-music').play();
}
function go() {
	play()
}
