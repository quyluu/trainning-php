let x = '';
let arr = [];
let str = '';
let count = 0;
let arrLocal = [];
function a(id) {
	return document.getElementById(id);
}
let answer = a('answer');
let answerResult = a('answer-result');

function set_num(number) {
	if(answer.value.endsWith('-0') == true || answer.value.endsWith('+0') == true || answer.value.endsWith('x0') == true || answer.value.endsWith('÷0') == true){
		x = x.slice(0, x.length - 1) + number;
		answer.value = x;
	}else{
		answer.value !== '0' ? x += number : x = number;
	}
	answer.value = x;

}

function cancel() {
	x = '';
	answerResult.value = 0;
	answer.value = x;
}

function del() {
	x = answer.value;
	x = x.slice(0, x.length - 1);
	answer.value = x;
}

function parentheses1() {
	(answer.value.endsWith('-') == true || answer.value.endsWith('+') == true || answer.value.endsWith('x') == true || answer.value.endsWith('÷') == true) ? x += '(' : x += '';
	answer.value = x;
}

function parentheses2() {
	arr = ((answer.value).split(''));
	for (i = 0; i < arr.length; i++) {
		if (arr[i] == '(') {
			count += 1;
		} else { count = count }
	}
	(answer.value.endsWith('-') == true || answer.value.endsWith('+') == true || answer.value.endsWith('x') == true || answer.value.endsWith('÷') == true || answer.value.endsWith(')') == true || answer.value.endsWith('(') == true || count % 2 == 0) ? x += '' : x += ')';
	answer.value = x;
}

function addition() {
	if (answer.value == '') {
		answer.value = '';
	} else
		if (answer.value.endsWith('-') == true || answer.value.endsWith('+') == true || answer.value.endsWith('x') == true || answer.value.endsWith('÷') == true) {
			x = x.slice(0, x.length - 1) + '+';
			answer.value = x;
		} else {
			answer.value += "+";
		}
	x = answer.value;
}

function subtraction() {
	if (answer.value.endsWith('--') == true || answer.value.endsWith('+-') == true || answer.value.startsWith('-') == true || answer.value.endsWith('x-') == true || answer.value.endsWith('÷-') == true) {
		x = answer.value;
	} else {
		answer.value += "-";
		x = answer.value;
	}
}

function multiplication() {
	if (answer.value == '') {
		answer.value = '';
	} else
		if (answer.value.endsWith('--') == true || answer.value.endsWith('+-') == true) {
			x = x.slice(0, x.length - 2) + 'x';
			answer.value = x;
		} else
			if (answer.value.endsWith('÷') == true || answer.value.endsWith('+') == true || answer.value.endsWith('x') == true || answer.value.endsWith('-') == true) {
				x = x.slice(0, x.length - 1) + 'x';
				answer.value = x;
			}
			else {
				answer.value += "x";
			}
	x = answer.value;
}

function division() {
	if (answer.value == '') {
		answer.value = '';
	} else
		if (answer.value.endsWith('--') == true || answer.value.endsWith('+-') == true) {
			x = x.slice(0, x.length - 2) + '÷';
			answer.value = x;
		} else
			if (answer.value.endsWith('÷') == true || answer.value.endsWith('+') == true || answer.value.endsWith('x') == true || answer.value.endsWith('-') == true) {
				x = x.slice(0, x.length - 1) + '÷';
				answer.value = x;
			}
			else {
				answer.value += "÷";
			}
	x = answer.value;
}

function dot() {
	str = answer.value;
	strIndexOf = str.indexOf(".");
	lastStrIndexOf = str.lastIndexOf(".");
	if (strIndexOf < 0 || (((strIndexOf >= 0 && str.slice(lastStrIndexOf + 1, -1).indexOf('+') >= 0) || (strIndexOf >= 0 && str.slice(lastStrIndexOf + 1, -1).indexOf('-') >= 0) || (strIndexOf >= 0 && str.slice(lastStrIndexOf + 1, -1).indexOf('x') >= 0) || (strIndexOf >= 0 && str.slice(lastStrIndexOf + 1, -1).indexOf(':') >= 0)) && answer.value.endsWith('.') == false)) {
		answer.value += ".";
	} else {
		answer.value = answer.value;
	}
	x = answer.value;
}

function sqrt() {
	if (answer.value == '') {
		answer.value = '';	
	} else {
		equal();
		x = Math.sqrt(answerResult.value);
		answer.value = x;
		answerResult.value = x;
	}
}

function sqr() {
	if (answer.value == '') {
		answer.value = '';
	} else {
		equal();
		x = answerResult.value;
		x *= x;
		answer.value = x;
		answerResult.value = x;
	}
}

function over() {
	if (answer.value != '') {
		equal();
		x = 1/answerResult.value;
		answerResult.value = x;
		answer.value = x;
	}else{
		x= '';
		answerResult.value = x;
	}
}

function equal() {
	if(answer.value == ''){return};
	x = answer.value;
	let kq = x.replace(/x/g, '*').replace(/÷/g, '/').replace(/--/g, '+');
	kq = parseFloat(eval(kq));
	answerResult.value = eval(kq);
	console.log(typeof kq);
	
	if(!isFinite(kq) || isNaN(kq) || typeof(kq)==undefined){
		return false;
	}
	arrLocal.push([answer.value,eval(kq)]);
	let arrLength = arrLocal.length;
	document.getElementById("list-history").innerHTML += `<li>` + arrLocal[arrLength-1][0] + " = " + "<b>" + arrLocal[arrLength-1][1] + '</b>' + `</li>`; 
}

