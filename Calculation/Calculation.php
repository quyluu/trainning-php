<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/calculation.css">
    <title>Calculation</title>
</head>
<body>
	<div class="calculator-wrap">
		<div id="shaped">
			<div id="calculator"><p>C a l c u l a t o r</p></div>
			<div id="result">
				<input type="text" readonly="readonly" id="answer"></input>
				<input type="text" readonly="readonly" id="answer-result" maxlength="15"></input>
			</div>
			<div class="row">
				<button style="color:red" class="btn_style" onclick="cancel()">C</button>
				<button class="btn_style btn_over" value="" onclick="over()"><sup>1</sup>/<sub>x</sub></button>
				<button class="btn_style btn_sqr" value="<sup>2</sup>" onclick="sqr()">x<sup>2</sup></button>
				<button class="btn_style" value="√" onclick="sqrt()">√</button>

				<button class="btn_style" onclick="del()">DEL</button>
				<button class="btn_style" value="(" onclick="parentheses1()">(</button>
				<button class="btn_style" value=")" onclick="parentheses2()">)</button>
				<button class="btn_style" value="+" onclick="addition(this)">+</button>

				<button class="btn_style" value="1" onclick="set_num(this.value)">1</button>
				<button class="btn_style" value="2" onclick="set_num(this.value)">2</button>
				<button class="btn_style" value="3" onclick="set_num(this.value)">3</button>
				<button class="btn_style" value="-" onclick="subtraction()">-</button>

				<button class="btn_style" value="4" onclick="set_num(this.value)">4</button>
				<button class="btn_style" value="5" onclick="set_num(this.value)">5</button>
				<button class="btn_style" value="6" onclick="set_num(this.value)">6</button>
				<button class="btn_style" value="x" onclick="multiplication()">x</button>

				<button class="btn_style" value="7" onclick="set_num(this.value)">7</button>
				<button class="btn_style" value="8" onclick="set_num(this.value)">8</button>
				<button class="btn_style" value="9" onclick="set_num(this.value)">9</button>
				<button class="btn_style" value="/" onclick="division()">÷</button>

				<button class="btn_style" value="0" onclick="set_num(this.value)">0</button>
				<button class="btn_style" value="." onclick="dot()">.</button>
				<button class="btn_style btn_equal" onclick="equal()">=</button>
			</div>
		</div>
		<div class="history">
			<h5 class="text-center mb-0">H i s t o r y</h5>
			<div class="body-history style-3">
				<ul id="list-history">
				</ul>
			</div>
		</div>
	</div>
	
	<script src= "../js/calculation.js"></script>
</body>
</html>
