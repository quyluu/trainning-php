<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/all.min.css">
    <link rel="stylesheet" href="../css/bmi.css">
    <title>BMI</title>
</head>
<body>
    <?php
        session_start();
        function getBMI($wei, $hei){
            $resultBMI = round($wei/pow(($hei/100),2),1);
            return $resultBMI;
        }
        $name = "";
        $age = "";
        $sex ="";
        $height = "";
        $weight = "";
        $showBMI= "";
        $flag = true;
        if(isset($_POST["height"]) && isset($_POST["weight"])){
            $name = $_POST["name"];
            $age = $_POST["age"];
            $height = $_POST["height"];
            $weight = $_POST["weight"];
            $sex = $_POST["sex"];
            if(is_numeric($height) && is_numeric($weight)){
                $result = getBMI($weight, $height);
                $_SESSION['result'][] = [$name, $age, $weight, $height, $sex, $result];
                if($result < 17){
                    $value = 'gầy độ II (BMI < 17)';
                }else
                if( $result < 18.5){
                    $value = 'gầy độ I (17 <= BMI < 18.5)';
                }else
                if($result < 25){
                    $value = 'cân đối (18.5 <= BMI < 25)';
                }else
                if($result < 30){
                    $value = 'thừa cân (25 <= BMI < 30)';
                }else
                if($result < 35){
                    $value = 'béo phì độ I (30 <= BMI < 35)';
                }else
                if(35 <= $result){
                    $value = 'béo phì độ II (BMI >= 35)';
                }
                if($flag == true){
                    $showBMI = '<div><b>- Chỉ số BMI của bạn: '. $result . '</b></div>' . '<div><b>- Cơ thể '. $value .'</b></div>';
                }else{
                    $showBMI = '<p class="warning text-center">Dữ liệu đầu vào sai</p>';
                }
            }else{
                $showBMI = '<p class="warning text-center">Dữ liệu đầu vào sai</p>';
            }
        }
    ?>
    <div class="wrap-bmi m-auto">
        <div class="header">
            <h3 class="text-center" style="color:white">Chỉ số BMI</h3>
            <a href="" title="Xem lịch sử"><i class="fas fa-history"></i></a>
        </div>
        <div class="body-main">
            <b>Chỉ số khối cơ thể (BMI) được dùng để xác định mức độ gầy hay béo của cơ thể thông qua chiều cao và cân nặng.</b>
            <form class="mt-4" action="#" method="post">
                <div class="information">
                    <label for="name">Họ và tên:</label>
                    <input type="text" name="name" id="name" value="<?php echo $name ?>">
                </div>
                <div class="information">
                    <label for="height">Chiều cao(cm)*:</label>
                    <input type="text" name="height" id="height" value="<?php echo $height ?>">
                </div>
                <div class="information">
                    <label for="weight">Cân nặng(kg)*:</label>
                    <input type="text" name="weight" id="weight" value="<?php echo $weight ?>">
                </div>
                <div class="information">
                    <label for="age">Độ tuổi:</label>
                    <input type="number" name="age" id="age" value="<?php echo $age ?>">
                </div>
                <div class="information">
                    <label for="">Giới tính:</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="male" name="sex" value="Nam" class="custom-control-input" <?php ($sex == "Nam")? print"checked" : print"" ?>>
                        <label class="custom-control-label" for="male">Nam</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="female" name="sex" value="Nữ" class="custom-control-input" <?php ($sex == "Nữ")? print"checked" : print"" ?>>
                        <label class="custom-control-label" for="female">Nữ</label>
                    </div>
                </div>
                <input  class="view" type="submit" value="Tính BMI">
            </form>
            <div class="result mt-4">
                <?php
                    echo $showBMI;
                ?>
            </div>
        </div>
    </div>
    <div class="wrap-bmi m-auto">
        <div class="header">
            <h3 class="text-center" style="color:white">Chỉ số BMI</h3>
            <a href="" title="Xem lịch sử"><i class="fas fa-history"></i></a>
        </div>
        <div class="body-main history">
            <table id="table">
                <thead id="table_head">
                    <th id="list1" onclick="sort_name();">Họ Tên<i class="fas fa-sort"></i></th>
                    <th id="list2" onclick="sort_sex();">GT<i class="fas fa-sort"></i></th>
                    <th id="list3" onclick="sort_age();">Tuổi<i class="fas fa-sort"></i></th>
                    <th id="list4" onclick="sort_cm();">Cm<i class="fas fa-sort"></i></th>
                    <th id="list5" onclick="sort_kg();">Kg<i class="fas fa-sort"></i></th>
                    <th id="list6" onclick="sort_bmi();">BMI<i class="fas fa-sort"></i></th>
                </thead>
                <tbody id="table_body">
                    <tr class="item">
                        <td class="item_name">Lưu Văn Quý</td>
                        <td class="item_sex">Nam</td>
                        <td class="item_age">27</td>
                        <td class="item_cm">168</td>
                        <td class="item_kg">63</td>
                        <td class="item_bmi">22.5</td>
                    </tr>
                    <tr class="item">
                        <td class="item_name">Nguyễn Thị An</td>
                        <td class="item_sex">Nữ</td>
                        <td class="item_age">22</td>
                        <td class="item_cm">164</td>
                        <td class="item_kg">22</td>
                        <td class="item_bmi">20</td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" id="name_order" value="asc">
            <input type="hidden" id="sex_order" value="asc">
            <input type="hidden" id="age_order" value="asc">
            <input type="hidden" id="cm_order" value="asc">
            <input type="hidden" id="kg_order" value="asc"> 
            <input type="hidden" id="bmi_order" value="asc"> 
        </div>
    </div>
    <?php
    // session_destroy();
    echo '<pre>';
        print_r($_SESSION['result']); 
    echo '</pre>';
    ?>
    <script type="text/javascript" src="../js/bmi.js"></script>
</body>
</html>
