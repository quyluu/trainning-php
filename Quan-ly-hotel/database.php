<?php
    define("DB_SERVER", "localhost");
    define("DB_USER", "root");
    define("DB_PASS", "root");
    define("DB_NAME", "data_employee");

    function db_connect(){
        $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        mysqli_set_charset($connection, 'UTF8');
        return $connection;
    }

    $db = db_connect();

    function db_disconnect($connection){
        if(isset($connection)){
            mysqli_close($connection);
        }
    }

    function find_all_hotel(){
        global $db;
        $sql = "SELECT * FROM hotels ";
        $sql .= "ORDER BY name_hotel";
        $result = mysqli_query($db, $sql);
        return $result;
    }

    function insert_hotel($hotel){
        global $db;

        $sql = "INSERT INTO hotelS ";
        $sql .= "(name_hotel, address_hotel, unit_price, status_hotel) ";
        $sql .= "VALUES (";
        $sql .= "'" . $hotel['name'] . "',";
        $sql .= "'" . $hotel['address'] . "',";
        $sql .= "'" . $hotel['price'] . "',";
        $sql .= "'" . $hotel['status'] . "'";
        $sql .= ")";
        $result = mysqli_query($db, $sql);

        if($result){
            return true;
        }else{
            echo mysqli_error($db);
            db_disconnect($db);
            exit;
        }
    }

    function confirm_query_result($result){
        global $db;
        if(!$result){
            echo mysqli_error($db);
            db_disconnect($db);
            exit;
        }else{
            return $result;
        }
    }

    function find_hotel_by_id($id){
        global $db;

        $sql = "SELECT * FROM hotels ";
        $sql .= "WHERE hotel_id='" . $id . "'";
        $result = mysqli_query($db, $sql);
        confirm_query_result($result);
        $hotel = mysqli_fetch_assoc($result);
        mysqli_free_result($result);
        return $hotel;
    }

    function update_hotel($hotel){
        global $db; 

        $sql = "UPDATE hotels SET ";
        $sql .= "name_hotel='" . $hotel['name'] . "', ";
        $sql .= "address_hotel='" . $hotel['address'] . "', ";
        $sql .= "unit_price='" . $hotel['price'] . "', ";
        $sql .= "status_hotel='" . $hotel['status'] . "' ";
        $sql .= "WHERE hotel_id='" . $hotel['id'] . "' ";
        $sql .= "LIMIT 1";

        $result = mysqli_query($db, $sql);
        return confirm_query_result($result);
    }

    function delete_hotel($id){
        global $db;

        $sql = "DELETE FROM hotels ";
        $sql .= "WHERE hotel_id='" . $id . "' ";
        $sql .= "LIMIT 1";
        $result = mysqli_query($db, $sql);
        return confirm_query_result($result);
    }
?>
