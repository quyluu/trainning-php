<?php
	require_once ('database.php');

	function isFormValidate(){
		global $errors;
		return count($errors) == 0;
	}

	$status ="";
	if(isset($_POST["status"])){
		$status = $_POST["status"];
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/calculation.css">
	<title>New hotel</title>
</head>

<body>
	<h3 class="text-center my-4">Thêm mới khách sạn</h3>
	<form style="width:500px" class="m-auto" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
		<div class="form-group">
			<label for="name">Tên khách sạn</label>
			<input type="text" class="form-control" id="name" name="name"
			 value ="<?php echo(empty($_POST['name'])?'':$_POST['name']) ?>">
		</div>
		<div class="form-group">
			<label for="address">Địa chỉ</label>
			<input type="text" class="form-control" id="address" name="address"
			value ="<?php echo(empty($_POST['address'])?'':$_POST['address']) ?>">
		</div>
		<div class="form-group">
			<label for="price">Giá phòng</label>
			<input type="number" class="form-control" id="address" name="price" step="10000" min="200000"
			value ="<?php echo (empty($_POST['price'])?'':$_POST['price']) ?>">
		</div>
		<div class="form-group">
			<label>Trạng thái</label>
			<div>
			<div class="custom-control custom-radio custom-control-inline">
				<input type="radio" id="status1" name="status" value="1" class="custom-control-input" <?php ($status == '1')? print"checked" : print"" ?>>
				<label class="custom-control-label" for="status1">Còn phòng</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input type="radio" id="status2" name="status" value="2" class="custom-control-input" <?php ($status =='2')? print"checked" : print"" ?>>
				<label class="custom-control-label" for="status2">Hết phòng</label>
			</div>
			</div>
		</div>
		<p style="color:red"> <?php echo($_SERVER["REQUEST_METHOD"] == 'POST' && (empty($_POST['name']) || empty($_POST['price']) || empty($_POST['address']) || empty($_POST['status'])))? 'Vui lòng điền đầy đủ thông tin' : '' ?></p>
		<button type="submit" class="btn btn-primary">Thêm</button>
		<a href="index.php" class="btn btn-warning">Quay lại danh sách</a>
	</form>
	<?php if($_SERVER["REQUEST_METHOD"] == 'POST' && !empty($_POST['name']) && !empty($_POST['price']) && !empty($_POST['address']) && !empty($_POST['status'])): ?>
		<?php
			$hotel = [];
			$hotel['name'] = $_POST['name'];
			$hotel['address'] = $_POST['address'];
			$hotel['price'] = $_POST['price'];
			$hotel['status'] = $_POST['status'];
			$result = insert_hotel($hotel);
			$newHotelId = mysqli_insert_id($db);
		?>
	<?php endif; ?>
</body>

</html>