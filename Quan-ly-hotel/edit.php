<?php
	require_once ('database.php');
	require_once ('function.php');

	function isFormValidate(){
		global $errors;
		return count($errors) == 0;
	}

	$status ="";
	if(isset($_POST["status"])){
		$status = $_POST["status"];
	}

	if($_SERVER["REQUEST_METHOD"] == 'POST' && !empty($_POST['name']) && !empty($_POST['price']) && !empty($_POST['address']) && !empty($_POST['status'])){
		$hotel = [];
		$hotel['id'] = $_POST['id'];
		$hotel['name'] = $_POST['name'];
		$hotel['address'] = $_POST['address'];
		$hotel['price'] = $_POST['price'];
		$hotel['status'] = $_POST['status'];

		update_hotel($hotel);
		redirect_to('index.php');
	}else{
		if(!isset($_GET['id'])){
			redirect_to('index.php');
		}
		$id = $_GET['id'];
		$hotel = find_hotel_by_id($id);
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/calculation.css">
	<title>Edit hotel</title>
</head>

<body>
	<h3 class="text-center my-4">Sửa khách sạn</h3>
	<form style="width:500px" class="m-auto" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id"
			 value ="<?php echo(empty($_POST['id'])?$hotel['hotel_id']:$_POST['id']) ?>">
		</div>	
		<div class="form-group">
			<label for="name">Tên khách sạn</label>
			<input type="text" class="form-control" id="name" name="name"
			 value ="<?php echo(empty($_POST['name'])?$hotel['name_hotel']:$_POST['name']) ?>">
		</div>
		<div class="form-group">
			<label for="address">Địa chỉ</label>
			<input type="text" class="form-control" id="address" name="address"
			value ="<?php echo(empty($_POST['address'])?$hotel['address_hotel']:$_POST['address']) ?>">
		</div>
		<div class="form-group">
			<label for="price">Giá phòng</label>
			<input type="number" class="form-control" id="address" name="price" step="10000" min="200000"
			value ="<?php echo (empty($_POST['price'])?$hotel['unit_price']:$_POST['price']) ?>">
		</div>
		<div class="form-group">
			<label>Trạng thái</label>
			<div>
			<div class="custom-control custom-radio custom-control-inline">
				<input type="radio" id="status1" name="status" value="1" class="custom-control-input" <?php ($hotel['status_hotel'] == '1')? print"checked" : print"" ?>>
				<label class="custom-control-label" for="status1">Còn phòng</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input type="radio" id="status2" name="status" value="2" class="custom-control-input" <?php ($hotel['status_hotel'] =='2')? print"checked" : print"" ?>>
				<label class="custom-control-label" for="status2">Hết phòng</label>
			</div>
			</div>
		</div>
		<p style="color:red"> <?php echo($_SERVER["REQUEST_METHOD"] == 'POST' && (empty($_POST['name']) || empty($_POST['price']) || empty($_POST['address']) || empty($_POST['status'])))? 'Vui lòng điền đầy đủ thông tin' : '' ?></p>
		<button type="submit" class="btn btn-primary">Sửa</button>
		<a href="index.php" class="btn btn-warning">Quay lại danh sách</a>
	</form>
</body>

</html>