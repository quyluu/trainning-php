<?php
require_once ('database.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/calculation.css">
    <title>Quản lý khách sạn</title>
</head>
<body>
    <h3 class="text-center mt-4">Quản lý khách sạn</h3>
    <div class="text-right">
        <a href="new-hotel.php" class="btn btn-success text-white mr-5 mb-3">Thêm mới</a>
    </div>
    <table class="table">
        <thead class="thead-dark">
			<tr>
				<th>STT</th>
				<th>Tên khách sạn</th>
				<th>Địa chỉ</th>
				<th>Giá phòng đôi/ngày</th>
				<th>Trạng thái</th>
				<th>Hành động</th>
			</tr>
        </thead>
        <tbody>
			<?php
				$hotel_set = find_all_hotel();
				$count = mysqli_num_rows($hotel_set);
				for($i = 0; $i < $count; $i++):
					$hotel = mysqli_fetch_assoc($hotel_set);
			?>
				<tr>
					<th><?php echo $i + 1 ?></th>
					<td><?php echo $hotel['name_hotel'] ?></td>
					<td><?php echo $hotel['address_hotel'] ?></td>
					<td><?php echo number_format($hotel['unit_price']) ?></td>
					<td><?php echo(($hotel['status_hotel'] == 2)? 'Hết phòng' : 'Còn phòng'); ?></td>
					<td>
						<a class="btn btn-warning" href="<?php echo 'edit.php?id=' . $hotel['hotel_id']; ?>">Sửa</a>
						<a class="btn btn-danger" href="<?php echo 'delete.php?id=' . $hotel['hotel_id']; ?>">Xóa</a>
					</td>
				</tr>
			<?php
			endfor;
			mysqli_free_result($hotel_set)
			?>
        </tbody>
      </table>
</body>
</html>
<?php
  db_disconnect($db)
?>