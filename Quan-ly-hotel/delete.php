<?php
	require_once ('database.php');
	require_once ('function.php');

	if($_SERVER["REQUEST_METHOD"] == 'POST'){
		delete_hotel($_POST['id']);
		redirect_to('index.php');
	}else{
		if(!isset($_GET['id'])){
			redirect_to('index.php');
		}
		$id = $_GET['id'];
		$hotel = find_hotel_by_id($id);
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/calculation.css">
	<title>New hotel</title>
</head>

<body class="text-center">
	<h3 class="text-center my-4">Xóa khách sạn</h3>
	<h4>Bạn có muốn xóa khách sạn này?</h4>
	<p><span>Tên khách sạn: </span><?php echo $hotel['name_hotel'] ?></p>
	<p><span>Địa chỉ: </span><?php echo $hotel['address_hotel'] ?></p>
	<p><span>Giá theo ngày: </span><?php echo $hotel['unit_price'] ?></p>
	<p><span>Tình trạng: </span><?php echo ($hotel['status_hotel'] == '1')? 'Còn phòng':'Hết phòng' ?></p>
	<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
		<input type="hidden" name="id" value="<?php echo $hotel['hotel_id'] ?>">
		<button type="submit" name="submit" class="btn btn-danger">Xóa</button>
		<a href="index.php" class="btn btn-warning">Quay lại danh sách</a>
	</form>
</body>

</html>